#include "camera.h"



Camera::Camera(float fov/* = 45.f*/, float aspect_ratio/* = 4.f / 3.f*/, float znear/* = 0.001f*/, float zfar/* = 10000.f*/)
        : _fov(fov), _aspect_ratio(aspect_ratio), _znear(znear), _zfar(zfar) {
    _pos = glm::vec3(0, 0, 0);
    _up = glm::vec3(0, 1, 0);
    _right = glm::vec3(1, 0, 0);
    _look = glm::vec3(0, 0, 1);
    buildView();
    buildPerspective();
}

void Camera::setFieldOfView(float degrees) {
    _fov = degrees;
    _dirty_perspective = true;
}

void Camera::setFarClip(float distance) {
    _zfar = distance;
    _dirty_perspective = true;
}

void Camera::setNearClip(float distance) {
    _znear = distance;
    _dirty_perspective = true;
}

void Camera::setAspectRatio(float aspect) {
    _aspect_ratio = aspect;
    _dirty_perspective = true;
}

void Camera::translate(glm::vec3 translation) {
    _pos += (_right * translation.x) + (_up * translation.y) + (_look * translation.z);
    _dirty_view = true;
}

void Camera::moveTo(glm::vec3 pos) {
    _pos = pos;
    _dirty_view = true;
}

void Camera::yaw(float degrees) {
    _look = glm::rotate(_look, degrees, glm::vec3(0, 1, 0));
    _right = glm::rotate(_right, degrees, glm::vec3(0, 1, 0));
    _dirty_view = true;
}

void Camera::pitch(float degrees) {
    _look = glm::rotate(_look, degrees, _right);
    _up = glm::rotate(_up, degrees, _right);
    _dirty_view = true;
}

void Camera::lookAt(glm::vec3 target) {
    _look = glm::normalize(target - _pos);
    _dirty_view = true;
}

glm::vec3 Camera::getPos() {
    return _pos;
}

glm::mat4 Camera::getView() {
    if (_dirty_view) {
        buildView();
    }
    return _view;
}

glm::mat4 Camera::getPerspective() {
    if (_dirty_perspective) {
        buildPerspective();
    }
    return _persp;
}

void Camera::buildView() {
    _dirty_view = false;

    // re-orthogonalize
    _look = glm::normalize(_look);
    _up = glm::normalize(glm::cross(_right, _look));
    _right = glm::cross(_look, _up);

    _view = glm::lookAt(_pos, _pos + _look, _up);
}

void Camera::buildPerspective() {
    _dirty_perspective = false;
    _persp = glm::perspective(_fov, _aspect_ratio, _znear, _zfar);
}
