#include "sys.h"
#include "game.h"
#include <glfw/glfw3.h>
#include <unistd.h>
#include <iostream>
#include "log.h"
//#include "foundation/Logging.h"

GLFWwindow* _window = NULL;
int32_t _window_width = 800;
int32_t _window_height = 600;
const char* _window_title = "dirty";
double curr_cursor[2] = {0};
double prev_cursor[2] = {0};
bool first = true;

game::KeyState _game_key_state = {0};
game::CursorState _game_cursor_state = {0};

static void errorCallback(const int error, const char *description) {
    LOG_E("sys", "glfw error: " << error << " - " << description);    
    glfwSetWindowShouldClose(_window, GL_TRUE);
}

static void keyCallback(GLFWwindow *window, const int key, const int scancode, const int action, const int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(_window, GL_TRUE);
    }

    switch(key) {
        case GLFW_KEY_W: {
            _game_key_state.is_w_pressed = action;
            break;
        }
        case GLFW_KEY_A: {
            _game_key_state.is_a_pressed = action;
            break;
        }
        case GLFW_KEY_S: {
            _game_key_state.is_s_pressed = action;
            break;
        }
        case GLFW_KEY_D: {
            _game_key_state.is_d_pressed = action;
            break;
        }
        case GLFW_KEY_Q: {
            _game_key_state.is_q_pressed = action;
            break;
        }
        case GLFW_KEY_E: {
            _game_key_state.is_e_pressed = action;
            break;
        }
    }
}

static void framebufferResizeCallback(GLFWwindow *window, int width, int height) {
//    LOG_D("sys", "framebuffer resize -> width:" << width << " height:" << height);
    _window_width = width;
    _window_height = height;
}

static void cursorPosCallback(GLFWwindow* window, double x, double y) {
//    LOG_D("sys", "cursor pos -> x:" << x << ", y:" << y);
    curr_cursor[0] = x;
    curr_cursor[1] = y;
    
    /**
     * theres no way to get the mouse position before a mouse event happes.
     * glfwGetCursorPos() always return (0,0) even though first movement jumps
     * to some other value. To prevent the first delta from being gigantic, need to
     * catch first ever mouse event and set prev vals;
     */
    if(first) {
        prev_cursor[0] = x;
        prev_cursor[1] = y;
        first = false;
    }
}

static void mouseButtonCallback(GLFWwindow* window, int btn, int action, int mods) {
    LOG_D("sys", "mouse btn -> btn:" << btn << " action:" << action << " mods: " << mods);
}

static void cursorEnterCallback(GLFWwindow* window, int entered) {
    LOG_D("sys", "cursor enter -> " << entered);
    _game_cursor_state.entered = entered;
}

int main(int argc, char** argv) {
    int ret = -1;

    glfwSetErrorCallback(errorCallback);

    if(!glfwInit()) {
        return ret;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);    
    
    _window = glfwCreateWindow(_window_width, _window_height, _window_title, NULL, NULL);
     if (!_window) {
        glfwTerminate();
        return ret;
    }
    
    glfwMakeContextCurrent(_window);
    glfwSetFramebufferSizeCallback(_window, framebufferResizeCallback);
    glfwSetCursorPosCallback(_window, cursorPosCallback);
    glfwSetMouseButtonCallback(_window, mouseButtonCallback);
    glfwSetKeyCallback(_window, keyCallback);
    glfwSetCursorEnterCallback(_window, cursorEnterCallback);
//    glfwSwapInterval(0);
    glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwGetWindowSize(_window, &_window_width, &_window_height);
    
    while(!glfwWindowShouldClose(_window)) {
        glfwPollEvents();

        _game_cursor_state.delta_x = curr_cursor[0] - prev_cursor[0];
        _game_cursor_state.delta_y = curr_cursor[1] - prev_cursor[1];
        prev_cursor[0] = curr_cursor[0];
        prev_cursor[1] = curr_cursor[1];
        
        game::frame(_game_key_state, _game_cursor_state);
    
        
        glfwSwapBuffers(_window);
    }

    glfwDestroyWindow(_window);
    glfwTerminate();

    return ret;
}

namespace sys {
    void setWindowTitle(const char* title) {
        glfwSetWindowTitle(_window, title);
    }

    double getTime() {
        return glfwGetTime();
    }
}