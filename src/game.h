#pragma once

namespace game {
    struct KeyState {
        bool is_w_pressed;
        bool is_a_pressed;
        bool is_s_pressed;
        bool is_d_pressed;
        bool is_q_pressed;
        bool is_e_pressed;
    };

    struct CursorState {
        bool entered;
        double delta_x;
        double delta_y;
    };

    void frame(const KeyState &key_state, const CursorState &cursor_state);
}