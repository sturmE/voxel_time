#pragma once

#include <ft2build.h>
#include FT_FREETYPE_H
#include "log.h"
#include "gfx/gl/gfx_gl.h"

class FontManager {
private:
    FT_Library _ft;
public:
    void addFont(const char* location);
};
