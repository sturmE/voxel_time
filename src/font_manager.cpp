#include "font_manager.h"


void FontManager::addFont(const char *location) {
    FT_Error res = 0;
    res = FT_Init_FreeType(&_ft);
    if(res != FT_Err_Ok) {
        LOG_E("main", "FreeType Init Failed " << res);
    }


    //const char* location = "/Library/Fonts/Microsoft/Consolas.ttf";
    FT_Face face;
    res = FT_New_Face(_ft, location, 0, &face);
    if(res != FT_Err_Ok) {
        LOG_E("main", "Failed to load face: " << location << " " << res);
    }

    FT_Set_Pixel_Sizes(face, 16, 0);

    gfx::gl::Texture glyph_atlas;
    glyph_atlas.create2D(1, GL_R8, GL_RED, 256, 256, 0);

    FT_GlyphSlot glyph = face->glyph;
    int64_t max_row_height = 0;
    int64_t max_col_width = 0;
    uint32_t row = 0;
    uint32_t col = 0;
    uint32_t x_offset = 0;
    uint32_t y_offset = 0;
    for(uint32_t char_ascii = 32; char_ascii < 128; ++char_ascii) {
        res = FT_Load_Char(face, char_ascii, FT_LOAD_RENDER);
        if(res != FT_Err_Ok) {
            LOG_E("main", "Failed to load glyph: " << (char)char_ascii << " " << res);
            continue;
        }

        max_row_height = std::max(max_row_height, (int64_t)face->glyph->bitmap.rows);
        max_col_width = std::max(max_col_width, (int64_t)face->glyph->bitmap.width);
        x_offset = col * 16;
        y_offset = row * 16;
        if(face->glyph->bitmap.buffer != NULL) {
            glyph_atlas.update(x_offset, y_offset, 16, 16, face->glyph->bitmap.buffer);
        }
        ++col;
        if(col % 16 == 0) {
            col = 0;
            ++row;
        }
    }

    LOG_D("main", "h:" << max_row_height << " w:" << max_col_width);

    FT_Done_Face(face);

}