#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


namespace ChunkManager {
    namespace Constants {
        const float scalar1 = 16;
        const float scalar2 = 5.0f;
        const glm::uvec3 chunkVoxelDims = glm::uvec3(1*scalar1, 1*scalar1, 1*scalar1);
        const glm::vec3  voxelDims = glm::vec3(1.f*scalar2, 1.f*scalar2, 1.f*scalar2);
        const glm::vec3  chunkDims = glm::vec3(chunkVoxelDims.x *  voxelDims.x, chunkVoxelDims.y *  voxelDims.y, chunkVoxelDims.z *  voxelDims.z);
    };
    void init(glm::vec3 pos);
    void update();
    void render(glm::mat4* proj_view);
}


