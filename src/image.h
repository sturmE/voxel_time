#pragma once

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "log.h"

enum PixelFormat {
    GREY,
    GREY_ALPHA,
    RGB,
    RGBA
};

struct Image {    
    int32_t width;
    int32_t height;
    PixelFormat pixel_format;
    uint8_t* data;

    ~Image() {
        if(data) {
            stbi_image_free(data);
        }
    }
};

static bool loadImageFromFile(const char* fpath, Image* image) {
    int components = 0;
    uint8_t* data = stbi_load(fpath, &image->width, &image->height, &components, 0);
    if(!data) {
        LOG_D("image", "failed to load image " << fpath);
        return false;
    }

    image->data = data;
    LOG_D("image", fpath << ", w: " << image->width << " h:" << image->height << " comp:" << components);
    switch(components) {
        case 1: {
            image->pixel_format = GREY;
            break;
        }
        case 2: {
            image->pixel_format = GREY_ALPHA;
            break;
        };
        case 3: {
            image->pixel_format = RGB;
            break;
        };
        case 4: {
            image->pixel_format = RGBA;
            break;
        };
    }
    return true;
}