#pragma once

#include <noise/noise.h>

namespace Noise {
    struct Perlin {    
        noise::module::Perlin generator;

        float noise3D(float x, float y, float z, float scale) {
            return generator.GetValue(x*scale, y*scale, z*scale);                
        }
    };
}

