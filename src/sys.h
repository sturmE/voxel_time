#pragma once

namespace sys {
    void setWindowTitle(const char* title);

    double getTime();
}