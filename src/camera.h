#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>

class Camera {
private:
    glm::vec3 _up;
    glm::vec3 _right;
    glm::vec3 _look;
    glm::vec3 _pos;
    
    float _fov;
    float _zfar;
    float _znear;
    float _aspect_ratio;
    
    glm::mat4 _persp;
    glm::mat4 _view;

    bool _dirty_view;
    bool _dirty_perspective;
public:
    Camera(float fov = 45.f, float aspect_ratio = 4.f/3.f, float znear = 0.001f, float zfar = 10000.f);

    void setFieldOfView(float degrees);
    void setFarClip(float distance);
    void setNearClip(float distance);
    void setAspectRatio(float aspect);

    void translate(glm::vec3 translation);
    void moveTo(glm::vec3 pos);
    void yaw(float degrees);
    void pitch(float degrees);
    void lookAt(glm::vec3 target);

    glm::vec3 getPos();
    glm::mat4 getView();
    glm::mat4 getPerspective();
private:
    void buildView();
    void buildPerspective();
};