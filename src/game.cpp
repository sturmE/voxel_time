#include "game.h"
#include "log.h"
#include "gfx/gl/gfx_gl.h"
#include "chunk_manager.h"
#include "sys.h"
#include "image.h"
#include <sstream>
#include <glm/gtx/rotate_vector.hpp>
#include "camera.h"
#include "font_manager.h"

bool _initialized = false;
glm::vec3 eye, target, up;
glm::mat4 persp;
gfx::gl::Program program;
gfx::gl::Shader vertex_shader, fragment_shader;
gfx::gl::Texture skybox_texture;
gfx::gl::VertexBuffer vertexBuffer;
uint32_t frame_count = 0;
double curr_frame_time = 0;
double prev_frame_time = 0;
double accumulate = 0;
double min_frame_time = 9999999.0;
double max_frame_time = 0;    
double total_frame_count = 0;
double frame_time = 0;
float mouse_speed = 0.25f;
float walk_speed = 100.f;
Camera cam;
glm::vec2 curr_mouse_speed;

std::string asString(const glm::vec3& v) {
    std::stringstream ss;
    ss << "x:" << v.x << ", y:" << v.y << ", z:" << v.z;
    return ss.str();
}
std::string asString(const glm::vec2& v) {
    std::stringstream ss;
    ss << "x:" << v.x << ", y:" << v.y;
    return ss.str();
}

struct Vertex {
    glm::vec3 pos;   
};

void makeCube(Vertex* vertices, float x, float y, float z, float scale = 1.f, bool flip = true) {
    static float positions[6][6][3] = {
        { { -0.5, -0.5, 0.5 }, { 0.5, -0.5, 0.5 }, { 0.5, 0.5, 0.5 },
            { 0.5, 0.5, 0.5 }, { -0.5, 0.5, 0.5 }, { -0.5, -0.5, 0.5 } }, // front
        
        { { 0.5, -0.5, -0.5 }, { -0.5, -0.5, -0.5 }, { -0.5, 0.5, -0.5 },
            { -0.5, 0.5, -0.5 }, { 0.5, 0.5, -0.5 }, { 0.5, -0.5, -0.5 } }, // back
        
        { { -0.5, -0.5, -0.5 }, { -0.5, -0.5, 0.5 }, { -0.5, 0.5, 0.5 },
            { -0.5, 0.5, 0.5 }, { -0.5, 0.5, -0.5 }, { -0.5, -0.5, -0.5 } }, // left
        
        { { 0.5, -0.5, 0.5 }, { 0.5, -0.5, -0.5 }, { 0.5, 0.5, -0.5 },
            { 0.5, 0.5, -0.5 }, { 0.5, 0.5, 0.5 }, { 0.5, -0.5, 0.5 } }, // right
        
        { { -0.5, 0.5, 0.5 }, { 0.5, 0.5, 0.5 }, { 0.5, 0.5, -0.5 },
            { 0.5, 0.5, -0.5 }, { -0.5, 0.5, -0.5 }, { -0.5, 0.5, 0.5 } }, // top
        
        { { 0.5, -0.5, 0.5 }, { -0.5, -0.5, 0.5 }, { -0.5, -0.5, -0.5 },
            { -0.5, -0.5, -0.5 }, { 0.5, -0.5, -0.5 }, { 0.5, -0.5, 0.5 } }  // bottom
    };  

    static float tex_coords[6][2] = {
        {0.f, 0.f }, //bl
        {1.f, 0.f }, //br
        {1.f, 1.f }, //tr
        {1.f, 1.f }, //tr
        {0.f, 1.f }, //tl
        {0.f, 0.f }  //bl     
    };   
    
    for (uint32_t f = 0; f < 6; ++f) { // for each face
        for (uint32_t v = 0; v < 6; ++v) { // for each vertex
            vertices->pos[0] = scale * positions[f][flip ? 5 - v : v][0] + x;
            vertices->pos[1] = scale * positions[f][flip ? 5 - v : v][1] + y;
            vertices->pos[2] = scale * positions[f][flip ? 5 - v : v][2] + z;
            
          //  vertices->tex[0] = tex_coords[v][0];
          //  vertices->tex[1] = tex_coords[v][1];
                    
            vertices++;
        }
    }
}



void init() {
    FontManager fm;
    fm.addFont("/Library/Fonts/Microsoft/Consolas.ttf");

    LOG_D("gfx", glGetString(GL_VERSION));
    LOG_D("gfx", glGetString(GL_SHADING_LANGUAGE_VERSION));
    LOG_D("gfx", glGetString(GL_VENDOR));
    LOG_D("gfx", glGetString(GL_RENDERER));
    glEnable (GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


    sys::setWindowTitle("gfx | FPS: 0");
    ChunkManager::init(glm::vec3(0, 0, 0));
    glClearColor(0.f, 0.f, 0.f, 1.f);
    uint32_t frame_count = 0;
    curr_frame_time = sys::getTime();
    prev_frame_time = curr_frame_time;
    accumulate = 0;
    min_frame_time = 9999999.0;
    max_frame_time = 0;    
    total_frame_count = 0;

    Vertex vertices[36];
    makeCube(&vertices[0], 0.f, 0.f, 0.f, 2.f, false);
    gfx::VertLayout layout;
    layout.add(gfx::ParamType::Float3);
    
    vertexBuffer.create(&layout, vertices, layout.stride * 36);

    LOG_D("main", "loading sykbox images");
    Image skybox_images[6] = {0};
    if(!loadImageFromFile("/Users/eugene.sturm/projects/misc/voxel_time/skybox/TropicalSunnyDayLeft2048.png", &skybox_images[0])) { LOG_D("main", "failed to load image"); }
    if(!loadImageFromFile("/Users/eugene.sturm/projects/misc/voxel_time/skybox/TropicalSunnyDayRight2048.png", &skybox_images[1])) { LOG_D("main", "failed to load image"); }
    if(!loadImageFromFile("/Users/eugene.sturm/projects/misc/voxel_time/skybox/TropicalSunnyDayUp2048.png", &skybox_images[2])) { LOG_D("main", "failed to load image"); }
    if(!loadImageFromFile("/Users/eugene.sturm/projects/misc/voxel_time/skybox/TropicalSunnyDayDown2048.png", &skybox_images[3])) { LOG_D("main", "failed to load image"); }
    if(!loadImageFromFile("/Users/eugene.sturm/projects/misc/voxel_time/skybox/TropicalSunnyDayFront2048.png", &skybox_images[4])) { LOG_D("main", "failed to load image"); }
    if(!loadImageFromFile("/Users/eugene.sturm/projects/misc/voxel_time/skybox/TropicalSunnyDayBack2048.png", &skybox_images[5])) { LOG_D("main", "failed to load image"); }

    LOG_D("main", "w: " << skybox_images[0].width << " h:" << skybox_images[0].height);

    void* datas[6] = { 
        skybox_images[0].data, 
        skybox_images[1].data, 
        skybox_images[2].data, 
        skybox_images[3].data,
        skybox_images[4].data,
        skybox_images[5].data 
    };

    
    skybox_texture.createCube(0, GL_UNSIGNED_BYTE, GL_RGB, skybox_images[0].width, skybox_images[0].height, datas);  

    static const GLchar* vertexSource[] = {
        "#version 410                               \n"
        "layout (location = 0) in vec3 pos;    \n"        
        "layout (location = 1) in vec2 tex;    \n"        
        "uniform mat4 world;                        \n"        
        "out vec3 tex_coord0;                        \n"
        "void main() {                              \n"
        "   vec4 world_pos = world * vec4(pos, 1.f);      \n"
        "   gl_Position = world_pos.xyww;      \n"
        "   tex_coord0 = pos;      \n"        
        
        "}                                          \n"
    };
    
    static const GLchar* fragmentSource[] = {
        "#version 410                               \n"
        "in vec3 tex_coord0;                            \n"
        "uniform samplerCube cube_texture;                            \n"
        "out vec4 color;                            \n"
        "void main() {                              \n"
        "   color = texture(cube_texture, tex_coord0);      \n"
        "}                                          \n"
    };

    
        
    vertex_shader.create(GL_VERTEX_SHADER, vertexSource);
    fragment_shader.create(GL_FRAGMENT_SHADER, fragmentSource);
    program.create(vertex_shader, fragment_shader);

    cam.moveTo(glm::vec3(0, 10, 500));
    cam.lookAt(glm::vec3(0, 0, 0));
}



namespace game {
    void frame(const KeyState  &key_state, const CursorState &cursor_state) {
        double dt = frame_time;

        if (!_initialized) {
            _initialized = true;
            init();
        }

        glm::vec3 translation(0, 0, 0);
        if (key_state.is_w_pressed) {
            translation.z += walk_speed * dt;
        }

        if (key_state.is_a_pressed) {
            translation.x -= walk_speed * dt;
        }

        if (key_state.is_s_pressed) {
            translation.z -= walk_speed * dt;
        }

        if (key_state.is_d_pressed) {
            translation.x += walk_speed * dt;
        }

        if (key_state.is_q_pressed) {
            translation.y += walk_speed * dt;
        }

        if (key_state.is_e_pressed) {
            translation.y -= walk_speed * dt;
        }

        cam.translate(translation);
        if (translation != glm::vec3(0, 0, 0)) {

        }

        glm::vec2 mouse_delta(cursor_state.delta_x, cursor_state.delta_y);
        if(mouse_delta.x != 0 || mouse_delta.y != 0) {
            float pitch = mouse_speed * dt * mouse_delta.y;
            float yaw = mouse_speed * dt * mouse_delta.x;
            cam.pitch(pitch);
            cam.yaw(-yaw);
        }

    
        glm::mat4 translate = glm::translate(glm::mat4(), cam.getPos());
        glm::mat4 scale = glm::scale(glm::mat4(), glm::vec3(50.f, 50.f, 50.f));
        glm::mat4 model = translate * scale;

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glm::mat4 persp_view = cam.getPerspective() * cam.getView();
        ChunkManager::render(&persp_view);
        
        glDepthFunc(GL_LEQUAL);
        glCullFace(GL_FRONT);
        gfx::gl::RenderTask task;
        task.vertex_buffer = vertexBuffer;
        task.program = program;
        task.uniform_data["cube_texture"] = std::make_pair(gfx::ParamType::Int32, &skybox_texture.slot);
        task.uniform_data["world"] = std::make_pair(gfx::ParamType::Float4x4,  (void*)glm::value_ptr(persp_view * model));
        task.vertex_count = 36;
        task.vertex_start = 0;
        gfx::gl::submit(task);
        glDepthFunc(GL_LESS);
        glCullFace(GL_BACK);
      
        

        prev_frame_time = curr_frame_time;
        curr_frame_time = sys::getTime();
        frame_time = curr_frame_time - prev_frame_time;
        accumulate += frame_time;
        if(frame_time > max_frame_time) max_frame_time = frame_time;
        if(frame_time < min_frame_time) min_frame_time = frame_time;
        ++frame_count;
        ++total_frame_count;

        if(accumulate > 1.0) {
            std::stringstream ss;
            ss << "gfx | FPS: " << frame_count << " | Max:" << max_frame_time << " | Min:" << min_frame_time << " | Frame: " << total_frame_count << " | Eye: " << cam.getPos().x << "," << cam.getPos().y << "," << cam.getPos().z;
            sys::setWindowTitle(ss.str().c_str());
            frame_count = 0;
            accumulate = 0.0;
        }
    }
}