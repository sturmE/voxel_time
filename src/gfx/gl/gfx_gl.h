#pragma once

#include <OpenGL/gl3.h>
#include "../../log.h"
#include "../vert_layout.h"
#include "../attrib_layout.h"
#include <unordered_map>
#include <functional>

#ifndef NDEBUG
#   define GL_CHECK(func) \
do { \
func;\
GLenum err = glGetError(); \
if(err != GL_NO_ERROR) { LOG_E("gfx", "GL error " << err << "("<<glEnumName(err)<<"): " << #func); }  \
}  \
while (false)
#else
#   define GL_CHECK(func) func
#endif


#define STRINGIFY(x) #x
static const char* glEnumName(GLenum _enum) {
#define GLENUM(_ty) case _ty: return #_ty
    
    switch (_enum)
    {
            GLENUM(GL_TEXTURE);
            GLENUM(GL_RENDERBUFFER);
            
            GLENUM(GL_INVALID_ENUM);
            GLENUM(GL_INVALID_VALUE);
            GLENUM(GL_INVALID_OPERATION);
            GLENUM(GL_OUT_OF_MEMORY);
            
            GLENUM(GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT);
            GLENUM(GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT);
            //			GLENUM(GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER);
            //			GLENUM(GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER);
            GLENUM(GL_FRAMEBUFFER_UNSUPPORTED);
            GLENUM(GL_FLOAT_VEC2);
            GLENUM(GL_FLOAT_VEC3);
            GLENUM(GL_FLOAT_VEC4);
            GLENUM(GL_FLOAT_MAT2);
            GLENUM(GL_FLOAT_MAT3);
            GLENUM(GL_FLOAT_MAT4);
            GLENUM(GL_SAMPLER_2D);
            GLENUM(GL_SAMPLER_CUBE);
    }
    return "";
}

template <class T>
void hash_combine(std::size_t& seed, const T& v)
{
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

namespace gfx {
    
    namespace gl {
        
        class VertexArrayObjectGL {
        private:
            GLuint _vao;
        public:
            VertexArrayObjectGL() : _vao(0) {}
            ~VertexArrayObjectGL() {
                if(_vao) {
                    glDeleteVertexArrays(1, &_vao);
                }
            }
            
            void create(){
                GL_CHECK(glGenVertexArrays(1, &_vao));
            }
            
            void bind() {
                GL_CHECK(glBindVertexArray(_vao));
            }
        };
        
        class VaoCache {
        private:
            std::unordered_map<uint32_t, VertexArrayObjectGL*> _map;
            VertexArrayObjectGL _vao[16];
            uint32_t _idx;
        public:
            VaoCache() : _idx(0) {}
            
            VertexArrayObjectGL* add(uint32_t hash) {
                VertexArrayObjectGL* vao = &_vao[_idx++];
                vao->create();
                _map.insert(std::make_pair(hash, vao));
                return vao;
            }
            
            VertexArrayObjectGL* find(uint32_t hash) {
                auto it = _map.find(hash);
                if(it != _map.end()) {
                    return it->second;
                }
                return nullptr;
            }
        };
        
        struct VertexBuffer {
            GLuint id = { 0 };
            VertLayout vertexLayout;
            
            void create(VertLayout* layout, void* data, size_t size, GLenum usage = GL_STATIC_DRAW) {
                GL_CHECK(glGenBuffers(1, &id));
                GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, id));
                GL_CHECK(glBufferData(GL_ARRAY_BUFFER, size, data, usage));
                vertexLayout = *layout;
            }
            
            void bind() const {
                GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, id));
            }
        };
        
        struct Shader {
            GLuint id = { 0 };
            GLenum type = { GL_FALSE };
            
            void create(GLenum shaderType, const char** src) {
                type = shaderType;
                id = glCreateShader(shaderType);
                
                GL_CHECK(glShaderSource(id, 1, (const GLchar**)src, NULL));
                GL_CHECK(glCompileShader(id));
                
                
                GLint compiled = 0;
                GL_CHECK(glGetShaderiv(id, GL_COMPILE_STATUS, &compiled) );
                
                if (compiled == 0) {
                    GLsizei len;
                    char log[1024];
                    GL_CHECK(glGetShaderInfoLog(id, sizeof(log), &len, log) );
                    LOG_E("gfx", "Failed to compile shader. " << compiled << " : " << log);
                } else {
                    LOG_D("gfx", "Compiled Shader\n" << *src);
                }
            }
        };
        
        struct Uniform {
            enum Type {
                Vec2,
                Vec3,
                Vec4,
                Mat3,
                Mat4,
                Count
            };
        };
        
        
        struct Program {
            GLuint id = { 0 };
            AttribLayout attribLayout;
            
            void create(Shader vertexShader, Shader fragmentShader) {
                id = glCreateProgram();
                
                GL_CHECK(glAttachShader(id, vertexShader.id));
                GL_CHECK(glAttachShader(id, fragmentShader.id));
                
                GL_CHECK(glLinkProgram(id));
                
                GLint linked = 0;
                GL_CHECK(glGetProgramiv(id, GL_LINK_STATUS, &linked));
                if(!linked) {
                    char log[1024];
                    GL_CHECK(glGetProgramInfoLog(id, 1024, nullptr, log));
                    LOG_E("gfx", "Failed to link program: " << log);
                }
                
                GLint maxLen;
                GLint numAttribs = 0;
                GL_CHECK(glGetProgramiv(id, GL_ACTIVE_UNIFORMS, &numAttribs));
                GL_CHECK(glGetProgramiv(id, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxLen));
                LOG_D("gfx", "numattribs:" << numAttribs);
                char* name = new char[maxLen];
                for(int32_t idx = 0; idx < numAttribs; ++idx) {
                    GLint size;
                    GLenum type;
                    GL_CHECK(glGetActiveUniform(id, idx, maxLen, nullptr, &size, &type, name));
                    LOG_D("gfx", "Program uniform: " << name);
                }
                delete [] name;
                
                GL_CHECK(glGetProgramiv(id, GL_ACTIVE_ATTRIBUTES, &numAttribs));
                GL_CHECK(glGetProgramiv(id, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxLen));
                
                
                
                
                name = new char[maxLen];
                
                for(int32_t idx = 0; idx < numAttribs; ++idx) {
                    GLint size;
                    GLenum type;
                    
                    GL_CHECK(glGetActiveAttrib(id, idx, maxLen, NULL, &size, &type, name));
                    LOG_D("gfx", "Attribute [loc:" << idx << ", name:" << name << ", type:" << glEnumName(type) <<", size:" << size << "]");
                    
                    if(size == 1) {
                        if(type == GL_FLOAT_VEC2) {
                            attribLayout.add(ParamType::Float2, name, idx, size);
                        } else if(type == GL_FLOAT_VEC3) {
                            attribLayout.add(ParamType::Float3, name, idx, size);
                        } else if(type == GL_FLOAT_VEC4) {
                            attribLayout.add(ParamType::Float4, name, idx, size);
                        } else {
                            LOG_W("gfx", "Attribute not registered");
                        }
                    } else {
                        LOG_W("gfx", "Attribute not registered, size > 1");
                    }
                    
                }
                
                delete [] name;
            }
            
            void bind() const {
                GL_CHECK(glUseProgram(id));
            }
            
            bool bindAttributes(VertLayout* vertLayout) {
                if(!matches(&attribLayout, vertLayout)) {
                    return false;
                }
                
                size_t offset = 0;
                for(uint32_t idx = 0; idx < attribLayout.elements.size(); ++idx) {
                    AttribElement* attribElem = &attribLayout.elements[idx];
                    VertElement* vertElem = &vertLayout->elements[idx];
                    
                    GLenum type = GL_FALSE;
                    GLuint size = 0;
                    switch(vertElem->type) {
                        case ParamType::Float:
                            type = GL_FLOAT;
                            size = 1;
                            break;
                        case ParamType::Float2:
                            type = GL_FLOAT;
                            size = 2;
                            break;
                        case ParamType::Float3:
                            type = GL_FLOAT;
                            size = 3;
                            break;
                        case ParamType::Float4:
                            type = GL_FLOAT;
                            size = 4;
                            break;
                        default:
                            LOG_E("gfx", "Unknown vertex element type");
                            return false;
                    }
                    
                    LOG_D("gfx", "Binding attribute (size:" << size << ", type:" << type << ", stride:" << vertLayout->stride << ", offset: " << offset << ") to location:" << attribElem->location);
                    GL_CHECK(glEnableVertexAttribArray(attribElem->location));
                    GL_CHECK(glVertexAttribPointer(attribElem->location, size, type, GL_FALSE, vertLayout->stride, (const void*)offset));
                    offset += sizeOfParam(vertElem->type);
                }
                return true;
            }
        };

        struct Texture {
            GLuint id;
            GLuint slot;
            GLenum format;
            GLenum type;
            GLenum tex;

            void createCube(uint32_t tex_slot, GLenum tex_type, GLenum tex_format, uint32_t width, uint32_t height, void** data) {
                format = tex_format;
                type = tex_type;
                slot = tex_slot;
                tex = GL_TEXTURE_CUBE_MAP;
                GL_CHECK(glGenTextures(1, &id));
                bind();
                for(uint32_t side = 0; side < 6; ++side) {                       
                    GL_CHECK(glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + side, 0, format, width, height, 0, format, type, data[side]));                        
                }   

                GL_CHECK(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
                GL_CHECK(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
                GL_CHECK(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE));
                GL_CHECK(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
                GL_CHECK(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
            }
            
            void create2D(uint32_t tex_slot, GLenum tex_type, GLenum tex_format, uint32_t width, uint32_t height, void* data) {
                format = tex_format;
                type = tex_type;
                slot = tex_slot;
                tex = GL_TEXTURE_2D;
            
                GL_CHECK(glGenTextures(1, &id));
                bind();
                GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, type, data));
                
                GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
                GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
                GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE));
                GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
                GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
            }
            
            void update(uint32_t x_offset, uint32_t y_offset, uint32_t width, uint32_t height, void* data) {
                GL_CHECK(glTexSubImage2D(GL_TEXTURE_2D, 0, x_offset, y_offset, width, height, format, type, data));
            }
            
            void bind() {
                GL_CHECK(glActiveTexture(GL_TEXTURE0 + slot));
                GL_CHECK(glBindTexture(tex, id));
            }
        };
        
        enum DrawType {
            DRAW_ARRAYS
        };
        enum DrawMode {
            TRIANGLES
        };
        
        static VaoCache vao_cache;
        
        struct RenderTask {
            Program program;
            VertexBuffer vertex_buffer;
            
            Texture texture[16];
            uint32_t num_textures;
            
            // draw call data
            uint32_t vertex_count;
            uint32_t vertex_start;
            
            // Note(eugene): This is an utter hack.
            std::unordered_map<const char*, std::pair<ParamType, void*>> uniform_data;
        };
        

        static void submit(RenderTask &task) {
            Program *prog = &task.program;
            VertexBuffer *vb = &task.vertex_buffer;
            
            // find/create the correct vao
            {
                size_t hash = 0;
                hash_combine(hash, prog->id);
                hash_combine(hash, vb->id);
                VertexArrayObjectGL *vao = vao_cache.find(hash);
                if(vao) {
                    vao->bind();
                } else {
                    vao = vao_cache.add(hash);
                    vao->bind();
                    vb->bind();
                    if(!prog->bindAttributes(&vb->vertexLayout)) {
                        return;
                    }
                }
            }
            
            prog->bind();
            
            // update uniforms
            {
                for(auto it = task.uniform_data.begin(); it != task.uniform_data.end(); ++it) {
                    const char* uniform_name = it->first;
                    ParamType param_type = it->second.first;
                    void* data = it->second.second;
                    
                    GLint location = glGetUniformLocation(prog->id, uniform_name);
                    if(location < 0) {
                        LOG_E("gfx", "Failed to find uniform by name '" << uniform_name << "'");
                        continue;
                    }
                    
                    switch(param_type) {
                        case ParamType::Int32: {
                            GL_CHECK(glUniform1i(location, (int)*((int*)data)));
                            break;
                        }
                        case ParamType::Float4x4: {
                            GL_CHECK(glUniformMatrix4fv(location, 1, GL_FALSE, (float*)data));
                            break;
                        }
                        default: {
                            LOG_E("gfx", "Unsupported uniform type");
                        }
                    }
                }
            }
            
            GL_CHECK(glDrawArrays(GL_TRIANGLES, task.vertex_start, task.vertex_count));
        }
        
        static void submit(RenderTask *tasks, uint32_t count) {
            for(uint32_t idx = 0; idx < count; ++idx) {
                submit(tasks[idx]);
            }
        }
        
    }
}
