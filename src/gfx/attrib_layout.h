#pragma once

#include <vector>
#include <stdint.h>
#include "vert_layout.h"

namespace gfx {
    
    struct AttribElement {
        ParamType type;
        
        const char* name;
        uint32_t location;
        uint32_t size;
        
        AttribElement(ParamType type, const char* name, uint32_t location, uint32_t size)
        : type(type), name(name), location(location), size(size) {};
    };
    
    struct AttribLayout {
        std::vector<AttribElement> elements;
        
        void add(ParamType type, const char* name, uint32_t location, uint32_t size) {
            elements.push_back(AttribElement(type, name, location, size));
        }
    };
    
    static bool matches(AttribLayout* attribLayout, VertLayout* vertLayout) {
        if(attribLayout->elements.size() != vertLayout->elements.size()) {
            return false;
        }
        
        for(uint32_t idx = 0; idx < attribLayout->elements.size(); ++idx) {
            if(attribLayout->elements[idx].type != vertLayout->elements[idx].type) {
                return false;
            }
        }
        
        return true;
    }
}

