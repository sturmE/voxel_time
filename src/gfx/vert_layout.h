#pragma once

#include <vector>
#include <stdint.h>
#include "param_type.h"

namespace gfx {
    struct VertElement {
        ParamType type;
        
        VertElement(ParamType type) : type(type) {};
    };
    
    struct VertLayout {
        std::vector<VertElement> elements;
        size_t stride { 0 };
        
        void add(ParamType type) {
            elements.push_back(VertElement(type));
            stride += sizeOfParam(type);
        }
    };
}


