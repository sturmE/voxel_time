#include "chunk_manager.h"
#include "gfx/gl/gfx_gl.h"
#include "noise.h"
#include "gfx/vert_layout.h"

struct VoxelVertex {
    glm::vec3 pos;
    glm::vec3 norm;
    
    static gfx::VertLayout layout;
    static void init() {
        layout.add(gfx::ParamType::Float3);
        layout.add(gfx::ParamType::Float3);
    }

};

gfx::VertLayout VoxelVertex::layout;

enum VoxelType {
    AIR_VOXEL = 0,
    GROUND_VOXEL = 1
};

struct Voxel {
    uint32_t id;
};

struct Chunk {
    Voxel* voxels;
    glm::vec3 pos;
    
    uint32_t vertexStart;
    uint32_t vertexCount;
    gfx::gl::VertexBuffer vertexBuffer;
};

namespace ChunkManager {
    gfx::gl::Program _terrainProgram;
    
    static const GLchar* vertexSource[] = {
        "#version 410                               \n"
        "layout (location = 0) in vec3 position;    \n"
        "layout (location = 1) in vec3 normal;      \n"
        "uniform mat4 world;                        \n"
        "uniform mat4 normalTransform;                        \n"        
        "out vec3 n;                        \n"
        "void main() {                              \n"
        "   vec4 a = normalTransform * vec4(normal, 1.f);      \n"
        "   n = a.xyz;      \n"
        "   gl_Position = world * vec4(position, 1.f);      \n"
        "}                                          \n"
    };
    
    static const GLchar* fragmentSource[] = {
        "#version 410                               \n"
        "in vec3 n;                            \n"
        "out vec4 color;                            \n"
        "void main() {                              \n"
        "   color = clamp(dot(n, normalize(vec3(-1.f, 1.f, 1.f))), 0.25f, 1.f) * vec4(0.7f, 0.7f, 0.7f, 1.0);       \n"
        "}                                          \n"
    };
    
    // prototype
    void _buildChunk(Chunk* chunk);

    
    
    Chunk* _chunks;
    uint32_t _active;
    Noise::Perlin _perlin;
    
    void init(glm::vec3 pos) {
        VoxelVertex::init();
        gfx::gl::Shader vertexShader, fragmentShader;
        
        vertexShader.create(GL_VERTEX_SHADER, vertexSource);
        fragmentShader.create(GL_FRAGMENT_SHADER, fragmentSource);
        _terrainProgram.create(vertexShader, fragmentShader);
        
        _perlin.generator.SetSeed(12312312);

        _active = 9;       
                        
        _chunks = new Chunk[32];
        Chunk* chunk = &_chunks[0];
        chunk->pos = pos;
        _buildChunk(&_chunks[0]);

        chunk = &_chunks[1];
        chunk->pos.x = pos.x + Constants::chunkDims.x / 2.f;
        _buildChunk(chunk);

        chunk = &_chunks[2];
        chunk->pos.x = pos.x - Constants::chunkDims.x / 2.f;
        _buildChunk(chunk);


        chunk = &_chunks[3];
        chunk->pos.z = pos.z - Constants::chunkDims.z / 2.f;
        _buildChunk(chunk);

        chunk = &_chunks[4];
        chunk->pos.z = pos.z + Constants::chunkDims.z / 2.f;
        _buildChunk(chunk);

        chunk = &_chunks[5];
        chunk->pos.x = pos.x + Constants::chunkDims.x / 2.f;
        chunk->pos.z = pos.z + Constants::chunkDims.z / 2.f;
        _buildChunk(chunk);

        chunk = &_chunks[6];
        chunk->pos.x = pos.x + Constants::chunkDims.x / 2.f;
        chunk->pos.z = pos.z - Constants::chunkDims.z / 2.f;
        _buildChunk(chunk);

        chunk = &_chunks[7];
        chunk->pos.x = pos.x - Constants::chunkDims.x / 2.f;
        chunk->pos.z = pos.z + Constants::chunkDims.z / 2.f;
        _buildChunk(chunk);

        chunk = &_chunks[8];
        chunk->pos.x = pos.x - Constants::chunkDims.x / 2.f;
        chunk->pos.z = pos.z - Constants::chunkDims.z / 2.f;
        _buildChunk(chunk);

   
    }
    
    void update() {
     

    }
    
    void render(glm::mat4* proj_view) {

        std::vector<gfx::gl::RenderTask> tasks;
        static float r = 0;
        r += 0.01f;
        
        for(uint32_t idx = 0; idx < _active; ++idx) {
            gfx::gl::RenderTask task = {};
            Chunk* chunk = &_chunks[idx];
            glm::mat4 model = glm::translate(glm::rotate(glm::mat4(), r, glm::vec3(0, 1, 0)), chunk->pos);
            glm::mat4 normal_transform = glm::transpose(glm::inverse(model));
            
            task.uniform_data["world"] = std::make_pair(gfx::ParamType::Float4x4, (void*)glm::value_ptr(*proj_view * model));
            task.uniform_data["normalTransform"] = std::make_pair(gfx::ParamType::Float4x4, (void*)glm::value_ptr(normal_transform));
            task.vertex_start = chunk->vertexStart;
            task.vertex_count = chunk->vertexCount;
            task.program = _terrainProgram;
            task.vertex_buffer = chunk->vertexBuffer;
            
            tasks.push_back(task);
        }
        
        gfx::gl::submit(tasks.data(), tasks.size());
    }
    
    
    void makeCube(VoxelVertex* vertices, float x, float y, float z, float scale = 1.f) {
        static float positions[6][6][3] = {
            { { -0.5, -0.5, 0.5 }, { 0.5, -0.5, 0.5 }, { 0.5, 0.5, 0.5 },
                { 0.5, 0.5, 0.5 }, { -0.5, 0.5, 0.5 }, { -0.5, -0.5, 0.5 } }, // front
            
            { { 0.5, -0.5, -0.5 }, { -0.5, -0.5, -0.5 }, { -0.5, 0.5, -0.5 },
                { -0.5, 0.5, -0.5 }, { 0.5, 0.5, -0.5 }, { 0.5, -0.5, -0.5 } }, // back
            
            { { -0.5, -0.5, -0.5 }, { -0.5, -0.5, 0.5 }, { -0.5, 0.5, 0.5 },
                { -0.5, 0.5, 0.5 }, { -0.5, 0.5, -0.5 }, { -0.5, -0.5, -0.5 } }, // left
            
            { { 0.5, -0.5, 0.5 }, { 0.5, -0.5, -0.5 }, { 0.5, 0.5, -0.5 },
                { 0.5, 0.5, -0.5 }, { 0.5, 0.5, 0.5 }, { 0.5, -0.5, 0.5 } }, // right
            
            { { -0.5, 0.5, 0.5 }, { 0.5, 0.5, 0.5 }, { 0.5, 0.5, -0.5 },
                { 0.5, 0.5, -0.5 }, { -0.5, 0.5, -0.5 }, { -0.5, 0.5, 0.5 } }, // top
            
            { { 0.5, -0.5, 0.5 }, { -0.5, -0.5, 0.5 }, { -0.5, -0.5, -0.5 },
                { -0.5, -0.5, -0.5 }, { 0.5, -0.5, -0.5 }, { 0.5, -0.5, 0.5 } }  // bottom
        };
        
        static float normals[6][3] = {
            { 0, 0, 1 }, // front
            { 0, 0, -1 }, // back
            { -1, 0, 0 }, // left
            { 1, 0, 0 }, // right
            { 0, 1, 0 }, // top
            { 0, -1, 0 }  // bottom
        };
        
        for (uint32_t f = 0; f < 6; ++f) { // for each face
            for (uint32_t v = 0; v < 6; ++v) { // for each vertex
                vertices->pos[0] = scale * positions[f][v][0] + x;
                vertices->pos[1] = scale * positions[f][v][1] + y;
                vertices->pos[2] = scale * positions[f][v][2] + z;
                
                vertices->norm[0] = normals[f][0];
                vertices->norm[1] = normals[f][1];
                vertices->norm[2] = normals[f][2];
                
                vertices++;
            }
        }
    }   


    
    inline uint32_t getIndex(uint32_t x, uint32_t y, uint32_t z) {
        return x + (y * Constants::chunkVoxelDims.y) + (z * Constants::chunkVoxelDims.y * Constants::chunkVoxelDims.z);
    }

    void _buildChunk(Chunk* chunk) {
        LOG_D("chunk", "----------Building chunk at pos " << chunk->pos.x << ", " << chunk->pos.y << ", " << chunk->pos.z << "----------------");
        float noiseScale = 0.01f;
        uint32_t groundVoxelCount = 0;
        
        // build all the voxels
        {
            uint32_t chunkVoxelCount = Constants::chunkVoxelDims.x * Constants::chunkVoxelDims.y * Constants::chunkVoxelDims.z;
            chunk->voxels = new Voxel[chunkVoxelCount];
            float z = chunk->pos.z + ((Constants::chunkDims.z / 2.f) - (Constants::voxelDims.z / 2.f));
            for (uint32_t k = 0; k < Constants::chunkVoxelDims.z; ++k) {
                float y = chunk->pos.y + ((Constants::chunkDims.y / 2.f) - (Constants::voxelDims.y / 2.f));
                for (uint32_t j = 0; j < Constants::chunkVoxelDims.y; ++j) {
                    float x = chunk->pos.x + ((-1.f * Constants::chunkDims.x / 2.f) + (Constants::voxelDims.x / 2.f));
                    for (uint32_t i = 0; i < Constants::chunkVoxelDims.x; ++i) {
                        float density = _perlin.noise3D(x, y, z, noiseScale);
                        //LOG_D("chunk", "x:" << x << " y:" << y << " z:" << z << " -> " << density);
                        uint32_t idx = i + (j * Constants::chunkVoxelDims.y) + (k * Constants::chunkVoxelDims.y * Constants::chunkVoxelDims.z);
                        if (density >= 0.f) {
                            chunk->voxels[idx].id = GROUND_VOXEL;
                            groundVoxelCount++;
                        }
                        else {
                            chunk->voxels[idx].id = AIR_VOXEL;
                        }
                        
                        x += Constants::voxelDims.x * 1;
                    }
                    y -= Constants::voxelDims.y * 1;
                }
                z -= Constants::voxelDims.z * 1;
            }
        } // end build voxels


/*
        {
            for(uint32_t a = 0; a < 2; ++a) { // front/back
                for(uint32_t b = 0; b < 3; ++b) { // dimension
                    // setup starting coordinates
                    uint32_t i = 0, j = 0, k = 0;

                    while(i < 1) {
                        while(chunk->voxels[getIndex(i, j, k)].type != AIR_VOXEL && ++k < Constants::chunkVoxelDims.z) {}

                        if(k == Constants::chunkVoxelDims.z) {
                            // nothing here in this line
                        }


                    }
                }
            }
        }
        */


        // build the mesh
        {
            uint32_t verticesPerVoxel = 36;
            uint32_t vertexCount = groundVoxelCount * verticesPerVoxel;
            VoxelVertex* vertices = new VoxelVertex[vertexCount];
            VoxelVertex* vertPtr = &vertices[0];
            float z = chunk->pos.z + ((Constants::chunkDims.z / 2.f) - (Constants::voxelDims.z / 2.f));
            for (uint32_t k = 0; k < Constants::chunkVoxelDims.z; ++k) {
                float y = chunk->pos.y + ((Constants::chunkDims.y / 2.f) - (Constants::voxelDims.y / 2.f));
                for (uint32_t j = 0; j < Constants::chunkVoxelDims.y; ++j) {
                    float x = chunk->pos.x + ((-1.f * Constants::chunkDims.x / 2.f) + (Constants::voxelDims.x / 2.f));
                    for (uint32_t i = 0; i < Constants::chunkVoxelDims.x; ++i) {
                        uint32_t idx = getIndex(i, j, k);
                        if (chunk->voxels[idx].id != GROUND_VOXEL) {
                            continue;
                        }
                        
                        makeCube(vertPtr, x, y, z, Constants::voxelDims.x); // Note(eugene): need to scale cube size by the voxel dims were using. Right now, just assume all dims are equals
                        vertPtr += verticesPerVoxel;
                        
                        x += Constants::voxelDims.x * 1;
                    }
                    y -= Constants::voxelDims.y * 1;
                }
                z -= Constants::voxelDims.z * 1;
            }

            chunk->vertexBuffer.create(&VoxelVertex::layout, vertices, VoxelVertex::layout.stride * vertexCount);
            chunk->vertexCount = vertexCount;
            chunk->vertexStart = 0;
            
            delete [] vertices;
        } // end build mesh
    }
}
