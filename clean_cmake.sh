#!/bin/bash

rm cmake_install.cmake
rm CMakeCache.txt
rm Makefile
rm -rf dgfx.xcodeproj
rm -rf CMakeFiles
rm -rf CMakeScripts
rm -rf dgfx.build
rm -rf bin